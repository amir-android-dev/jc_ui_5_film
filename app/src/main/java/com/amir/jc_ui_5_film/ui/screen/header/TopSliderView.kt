package com.amir.jc_ui_5_film.ui.screen.header

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.amir.jc_ui_5_film.ui.theme.AppDark

@Composable
fun TopSliderView(
    sliderHeight: Dp,
    filters: List<String>
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(sliderHeight)
    ) {
        //top slider
        //big img
        SliderMainImageView( sliderHeight)
        //to give a gradient
        //top filter & search
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(sliderHeight)
                .background(Brush.verticalGradient(listOf(Color.Transparent, AppDark)))
                .align(Alignment.BottomCenter)
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(25.dp)
                .align(Alignment.TopCenter)
        ) {
            SliderTopFilterView(filters)
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
        ) {
            SliderBottomView()
        }
    }
}