package com.amir.jc_ui_5_film.ui.screen.header

import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import com.amir.jc_ui_5_film.ui.theme.AppGray

@Composable
 fun SliderTopFilterView(
    filters: List<String>,
) {
    var isFilterSelected by remember { mutableIntStateOf(0) }

    LazyRow {
        item {
            IconButton(onClick = { /*TODO*/ }) {
                Icon(
                    imageVector = Icons.Default.Search, contentDescription = "",
                    tint = Color.White
                )
            }
        }
        itemsIndexed(filters) { index, item ->
            TextButton(onClick = { isFilterSelected = index }) {
                Text(
                    text = item,
                    color = if (isFilterSelected == index) Color.White else AppGray
                )
            }
        }

    }
}