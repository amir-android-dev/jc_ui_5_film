package com.amir.jc_ui_5_film.ui.screen.header

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_5_film.R
import com.amir.jc_ui_5_film.ui.theme.AppGray
import com.amir.jc_ui_5_film.ui.theme.AppRed

@Composable
fun SliderBottomView() {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(25.dp),
    ) {
        Image(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = "NetFelix",
            Modifier.size(50.dp)
        )
        Column(Modifier.weight(1f)) {
            Text(text = "Available Now", fontSize = 14.sp, color = AppGray)
            Text(text = "Watch Joker", fontSize = 21.sp, color = Color.White)
        }
        IconButton(onClick = { /*TODO*/ }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_play_cicle),
                contentDescription = "Play",
                tint = AppRed,
                modifier = Modifier.size(50.dp)

            )
        }
    }
}