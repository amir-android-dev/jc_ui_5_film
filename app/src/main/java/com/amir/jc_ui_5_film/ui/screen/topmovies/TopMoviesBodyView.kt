package com.amir.jc_ui_5_film.ui.screen.topmovies

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_5_film.R
import com.amir.jc_ui_5_film.data.MockDataMovie
import com.amir.jc_ui_5_film.ui.theme.AppGold
import com.amir.jc_ui_5_film.ui.theme.AppGray
import com.amir.jc_ui_5_film.ui.theme.AppWhite

@Composable
fun TopMoviesBodyView() {
    val lazyListState = rememberLazyListState()
    LazyRow(
        state = lazyListState,
        contentPadding = PaddingValues(25.dp, 0.dp),
        horizontalArrangement = Arrangement.spacedBy(15.dp)
    ) {
        itemsIndexed(MockDataMovie.jokerListSlider()) { index, item ->
            Card(
                modifier = Modifier
                    .width(150.dp)
                    .height(230.dp)
                    .clip(RoundedCornerShape(15.dp)),
                elevation = 8.dp
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .clip(RoundedCornerShape(15.dp))
                ) {
                    Image(
                        painter = painterResource(id = item.img),
                        contentDescription = "movie image",
                        modifier = Modifier.fillMaxSize(),
                        contentScale = ContentScale.Crop
                    )
                    Box(
                        modifier = Modifier
                            .background(AppGray)
                            .padding(3.dp)
                            .align(Alignment.TopStart)
                    ) {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_star),
                                contentDescription = "rank",
                                tint = AppGold,
                                modifier = Modifier.size(20.dp)
                            )
                            Spacer(modifier = Modifier.width(1.dp))
                            Text(
                                text = item.rank.toString(),
                                fontSize = 14.sp,
                                color = AppWhite
                            )
                        }
                    }
                }
            }


        }

    }
}