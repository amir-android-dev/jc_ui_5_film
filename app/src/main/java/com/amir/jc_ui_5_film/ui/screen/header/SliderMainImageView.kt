package com.amir.jc_ui_5_film.ui.screen.header

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.snapping.rememberSnapFlingBehavior
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import com.amir.jc_ui_5_film.data.MockData

@Composable
@OptIn(ExperimentalFoundationApi::class)
fun SliderMainImageView(
    sliderHeight: Dp
) {
    val lazyListState = rememberLazyListState()
    LazyRow(
        state = lazyListState,
        flingBehavior = rememberSnapFlingBehavior(lazyListState = lazyListState),
        modifier = Modifier
            .fillMaxWidth()
            .height(sliderHeight)
    ) {
        items(MockData.jokerListSlider().size) {
            val item = MockData.jokerListSlider()[it]
            Box(Modifier.fillMaxSize()) {
                Image(
                    painter = painterResource(id = item.img),
                    contentDescription = "slider",
                    Modifier.fillParentMaxSize(),
                    contentScale = ContentScale.Crop
                )

            }
        }
    }
}