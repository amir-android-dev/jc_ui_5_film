package com.amir.jc_ui_5_film.ui.screen.topmovies

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_5_film.ui.theme.AppGray

@Composable
fun TopMoviesHeaderView() {
    Box(
        Modifier
            .fillMaxWidth()
            .padding(25.dp, 25.dp, 25.dp, 10.dp)
    ) {
        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(
                text = "Popular Movies", fontSize = 21.sp, color = Color.White,
                modifier = Modifier.weight(1f)
            )
            TextButton(onClick = { /*TODO*/ }) {
                Text(text = "View All", fontSize = 14.sp, color = AppGray)
            }

        }
    }
}