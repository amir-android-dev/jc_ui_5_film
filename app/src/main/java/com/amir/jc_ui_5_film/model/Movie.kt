package com.amir.jc_ui_5_film.model

import androidx.annotation.DrawableRes

data class Movie(@DrawableRes var img: Int, val rank: Float)
