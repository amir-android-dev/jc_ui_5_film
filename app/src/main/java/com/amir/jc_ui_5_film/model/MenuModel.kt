package com.amir.jc_ui_5_film.model

import androidx.annotation.DrawableRes

data class MenuModel(@DrawableRes var img: Int, val title: String)
