package com.amir.jc_ui_5_film.model

import androidx.annotation.DrawableRes

data class Slider(@DrawableRes var img: Int)
