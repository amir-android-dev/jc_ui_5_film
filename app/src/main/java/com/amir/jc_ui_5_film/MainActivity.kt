package com.amir.jc_ui_5_film

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_5_film.data.MenuItems
import com.amir.jc_ui_5_film.ui.screen.header.TopSliderView
import com.amir.jc_ui_5_film.ui.screen.topmovies.TopMoviesBodyView
import com.amir.jc_ui_5_film.ui.screen.topmovies.TopMoviesHeaderView
import com.amir.jc_ui_5_film.ui.theme.AppDark
import com.amir.jc_ui_5_film.ui.theme.AppLightGray
import com.amir.jc_ui_5_film.ui.theme.AppMenuGray
import com.amir.jc_ui_5_film.ui.theme.AppRed
import com.amir.jc_ui_5_film.ui.theme.AppWhite
import com.amir.jc_ui_5_film.ui.theme.Jc_ui_5_filmTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Jc_ui_5_filmTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainView()
                }
            }
        }
    }
}

@Composable
fun MainView() {

    val sliderHeight = 340.dp
    val filters = listOf("MOVIES", "TV SHOWS", "NETFLIX", "YOUTUBE", "TRAILERS")


    Box(
        Modifier
            .fillMaxSize()
            .background(AppDark)
    ) {
        Column(Modifier.fillMaxSize()) {
            TopSliderView(sliderHeight, filters)
            Spacer(modifier = Modifier.height(10.dp))
            TopMoviesHeaderView()
            TopMoviesBodyView()
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(70.dp)
                .clip(RoundedCornerShape(50.dp, 50.dp, 0.dp, 0.dp))
                .align(Alignment.BottomCenter)
        ) {
            BottomMenuView()
        }
    }
}

@Composable
fun BottomMenuView() {
    var isMenuSelected by remember { mutableIntStateOf(0) }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(AppLightGray)
    ) {
        LazyRow(
            Modifier
                .fillMaxSize()
                .padding(25.dp, 15.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            itemsIndexed(MenuItems.menuItems) { index, item ->
                IconButton(
                    onClick = { isMenuSelected = index }, Modifier.clip(
                        RoundedCornerShape(50.dp)
                    )
                ) {
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .background(if (isMenuSelected == index) AppRed else Color.Transparent)
                            .padding(10.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            painter = painterResource(id = item.img),
                            contentDescription = "",
                            tint = if (isMenuSelected == index) Color.White else AppMenuGray,
                            modifier = Modifier.size(25.dp)
                        )
                        //as default from android ButtonDefaults.IconSpacing
                        Spacer(modifier = Modifier.width(ButtonDefaults.IconSpacing))
                        if (isMenuSelected == index) {
                            Text(text = item.title, fontSize = 14.sp, color = AppWhite)
                        }

                    }
                }

            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Jc_ui_5_filmTheme {
        MainView()
    }
}