package com.amir.jc_ui_5_film.data

import com.amir.jc_ui_5_film.R
import com.amir.jc_ui_5_film.model.Movie
import com.amir.jc_ui_5_film.model.Slider

class MockDataMovie {

    companion object {
        private val movies = listOf(
            Movie(R.drawable.movie_1,8.4f),
            Movie(R.drawable.movie_2,5.4f),
            Movie(R.drawable.movie_3,9.4f),
            Movie(R.drawable.movie_4,6.4f),
            Movie(R.drawable.movie_5,3.4f),
            Movie(R.drawable.movie_6,7.4f),
            Movie(R.drawable.movie_7,84.4f)
        )

        fun jokerListSlider() = movies
    }
}