package com.amir.jc_ui_5_film.data

import com.amir.jc_ui_5_film.R
import com.amir.jc_ui_5_film.model.MenuModel

class MenuItems {

    companion object {
        private val menuList = listOf(
            MenuModel(R.drawable.ic_home, "Home"),
            MenuModel(R.drawable.ic_video, "Video"),
            MenuModel(R.drawable.ic_heart, "Favorites"),
            MenuModel(R.drawable.ic_user, "Profile")
        )

        val menuItems = menuList
    }


}