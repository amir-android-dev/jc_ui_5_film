package com.amir.jc_ui_5_film.data

import com.amir.jc_ui_5_film.R
import com.amir.jc_ui_5_film.model.Slider

class MockData {

    companion object {
        private val slider = listOf(
            Slider(R.drawable.joker0),
            Slider(R.drawable.joker1),
            Slider(R.drawable.joker2),
            Slider(R.drawable.joker3)
        )

        fun jokerListSlider() = slider
    }
}